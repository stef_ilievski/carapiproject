<?php 

include "includes/header.php";
session_start();
if(!isset($_SESSION['username'])) {
    header('Location: login.php');
}

?>

<div class="container my-4">
        <button class="btn btn-warning" id="getData">Display Data</button>
</div>

    <div class="container">

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
<!--                 <h4>Car Details
                    <a href="#" class="btn btn-primary float-end">Placeholder</a> -> button to see all manufacturers 
                </h4> -->
            <div class="card-body">
                <table class="table table-bordered table-striped" id="table_data">
                    <thead class="table-dark">
                        <tr>
                            <th>Name</th>
                            <th>Logo</th>
                            <th>Number of Models</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</div>


<?php  include "includes/footer.php"; ?>

<script>
$('#getData').click(function(){
    $.getJSON("https://api.npoint.io/dcfd5e5fcaaf38f7df81", function(data){
        var car_data = '';
        $.each(data, function(key,value){
                car_data += '<tr>';
                car_data += '<td>'+value.name+'</td>';
                car_data += '<td><img src='+value.img_url+' width="100" height="100"></td>'
                car_data += '<td>'+value.num_models+'</td>';
                car_data += '</tr>';
        });
        $('#table_data').append(car_data);
    });
});

</script>