<?php 

include "includes/header.php";
include "functions.php";

session_start();
if(!isset($_SESSION['username'])) {
    header('Location: login.php');
}

?>

<?php 

if(isset($_GET{'id'})) {
    $the_car_id = mysqli_real_escape_string($connect, $_GET['id']);
}

?>



<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>
                            Full Info
                        <a href="view_all_cm.php" class="btn btn-danger float-end">Go Back</a>
                    </h4>
                </div>
                </div>
                <div class="card-body">

                    <?php 

                    $query = "SELECT * FROM vehicles WHERE id = '$the_car_id'";
                    $query_run = mysqli_query($connect, $query);

                    while($row = mysqli_fetch_assoc($query_run)) {
                        $car_id = $row['id'];
                        $num_of_models = $row['num_models'];
                        $img_url = $row['img_url'];
                        $max_car_id = $row['max_car_id'];
                        $name = $row['name'];
                        $avg_horsepower = $row['avg_horsepower'];
                        $avg_price = $row['avg_price'];

                    }

                    ?>

                        <div class="mb-3">
                            <label>Name:</label>
                            <p class="form-control">
                            <?php echo $name; ?>
                            </p>
                         </div>
                         <div class="mb-3">
                            <label>Number of Models:</label>
                            <p class="form-control">
                            <?php echo $num_of_models; ?>
                            </p>
                         </div>
                         <div class="mb-3">
                            <label>Average Horsepower:</label>
                            <p class="form-control">
                            <?php echo ceil($avg_horsepower); ?>
                            </p>
                         </div>
                         <div class="mb-3">
                            <label>Average Price:</label>
                            <p class="form-control">
                            <?php echo round($avg_price, 2); ?>$
                            </p>
                         </div>
                         <div class="mb-3">
                            <label>Image</label>
                            <img width="100" height="100" src="<?php echo $img_url; ?>">        
                        </div>
                    
                   <?php  ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php 

if(isset($_POST['create_review'])) {
    $the_car_id = $_GET['id'];

    $review_author = $_POST['review_author'];
    $review_content = $_POST['review_content'];

    if(!empty($review_author) && !empty($review_content)) {
        $query = "INSERT INTO reviews (review_car_id, review_author, review_content, review_date) VALUES ($the_car_id, '{$review_author}', '{$review_content}', NOW())";
        $create_review = mysqli_query($connect, $query);
    

    if(!$create_review) {
        die("Review Query Failed". mysqli_error($connect));
    }

    $query = "UPDATE vehicles SET car_review_count = car_review_count + 1 WHERE id = $the_car_id";
    $update_review_count = mysqli_query($connect, $query);

} else {
    echo "<script>alert('Fields cannot be empty')</script>";
}

}

?>

<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4>Leave a review:</h4></div>
                    <div class="card-body">
                    <form action="" method="post" role="form">

                        <div class="form-group">
                            <label for="Author">Author</label>
                            <input type="text" class="form-control" name="review_author">
                        </div>

                        <div class="form-group">
                            <label for="Comment">Comment</label>
                            <textarea class="form-control" name="review_content" rows="3"></textarea>
                        </div>
                        <button type="submit" name="create_review" class="btn btn-primary mt-3">Submit</button>
                    </form>
                    </div>
                </div>
             </div>            
    </div>


    <?php 
    
    $query = "SELECT * FROM reviews WHERE review_car_id = {$the_car_id} ORDER BY review_id DESC";
    $select_reviews_query = mysqli_query($connect, $query);
    if(!$select_reviews_query) {
        die("Query Failed". mysqli_error($connect));
    }
    while($row = mysqli_fetch_assoc($select_reviews_query)) {
            $review_date = $row['review_date'];
            $review_author = $row['review_author'];
            $review_content = $row['review_content'];

            ?>

<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4><?php echo $review_author; ?></h4></div>
                    <div class="card-body">
                    <form action="" role="form">
                        <div class="form-group">
                            <label for="Date"><?php echo $review_date; ?></label>
                        </div>

                        <div class="form-group">
                            <textarea disabled class="form-control" name="review_content" rows="3"><?php echo $review_content; ?></textarea>
                        </div>
                    </form>
                    </div>
                </div>
             </div>            
    </div>        
   <?php }
    
    ?>


<?php include "includes/footer.php"; ?>