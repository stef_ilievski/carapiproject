<?php 

include "functions.php";
include "includes/header.php";


session_start();
if(!isset($_SESSION['username'])) {
    header('Location: login.php');
}



?>

<?php 

if(isset($_POST['favorite'])) {
    $car_id = $_POST['car_id'];
    $user_id = loggedInUserId();

    //1. SELECT CAR

    $query = "SELECT * FROM vehicles WHERE id = $car_id";
    $query_run = mysqli_query($connect, $query);
    $queryResult = mysqli_fetch_array($query_run);
    $favorites = $queryResult['favorites'];

    //2. UPDATE CAR WITH FAVORITES

    mysqli_query($connect, "UPDATE vehicles SET favorites=$favorites+1 WHERE id = $car_id");

    //3. INSERT/INCREMENT LIKES FOR CAR MAN.

    mysqli_query($connect, "INSERT INTO favorites (user_id, car_id) VALUES($user_id, $car_id)");
    exit();
}

if(isset($_POST['unfavorite'])) {

    $car_id = $_POST['car_id'];
    $user_id = loggedInUserId();

    //1. SELECT CAR

    $query = "SELECT * FROM vehicles WHERE id = $car_id";
    $query_run = mysqli_query($connect, $query);
    $queryResult = mysqli_fetch_array($query_run);
    $favorites = $queryResult['favorites'];

    //2. DELETE FAVORITES

    mysqli_query($connect, "DELETE FROM favorites WHERE car_id = $car_id AND user_id = $user_id");

    //3. UPDATE WITH DECREMENT FAVORITES

    mysqli_query($connect, "UPDATE vehicles SET favorites=$favorites-1 WHERE id = $car_id");
    exit();

}

?>

<div class="container mt-5">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h4 class="text-center"> Welcome: 
                    <?php echo $_SESSION['username']; ?>
                    <a href="logout.php" class="btn btn-dark float-end">Log out</a>
                </h4>
                </div>
                
                <div class="card-header">
                <h4>
                    Car Manufacturer Details
                </h4>
                </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Number of Models</th>
                            <th>Image</th>
                            <th>View all info</th>
                            <th>Favorites</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        
                        $query = "SELECT * FROM vehicles";
                        $select_cars = mysqli_query($connect, $query);

                        while($row = mysqli_fetch_assoc($select_cars)) {
                            $car_id = $row['id'];
                            $num_of_models = $row['num_models'];
                            $img_url = $row['img_url'];
                            $max_car_id = $row['max_car_id'];
                            $name = $row['name'];
                            $avg_horsepower = $row['avg_horsepower'];
                            $avg_price = $row['avg_price'];
                            $favorites = $row['favorites'];
                            $reviews = $row['car_review_count'];


                            echo "<tr>";
                            echo "<td>{$name}</td>";
                            echo "<td>{$num_of_models}</td>";
                            echo "<td><img src='{$img_url}' width='100' height='100' class='img-fluid'></td>";
                            echo "<td><a href='view_cm.php?id={$car_id}'class='btn btn-info btn-sm'>View</a></td>";
                            echo '<td>' ?> <i class="<?php echo userFavoritedThis($car_id) ? 'bi bi-star-fill' : 'bi bi-star' ?>" id="<?php echo $car_id; ?>">Add to Favorites</i>
                            <?php
                             echo '</td>';
                            echo "</tr>";

                        }
                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php 

include "includes/footer.php";

?>

<script>

$(document).ready(function(){

   

    var user_id = <?php echo loggedInUserId(); ?>


    $('i').click(function(){
        $clicked_btn = $(this);
        var car_id = $(this).attr("id");

        if($clicked_btn.hasClass('bi bi-star')) {
            $clicked_btn.removeClass('bi bi-star');
            $clicked_btn.addClass('bi bi-star-fill');
            alert("Successfully Added to Favorites");

            $.ajax({
            url: "view_all_cm.php",
            type: 'POST', 
            data: {
                'favorite': 1, 
                'car_id': car_id,
                'user_id': user_id
            }

        });

        } else if($clicked_btn.hasClass('bi bi-star-fill')) {
            $clicked_btn.removeClass('bi bi-star-fill');
            $clicked_btn.addClass('bi bi-star');
            alert("Successfully Removed from Favorites");
            

            $.ajax({
            url: "view_all_cm.php",
            type: 'POST', 
            data: {
                'unfavorite': 1, 
                'car_id': car_id,
                'user_id': user_id
            }
        });
        }


    });

});

</script>